[
  {
    "firstName": "Mike0",
    "lastName": "Cunneen0",
    "nickName": "Mikey0",
    "title": "Mr",
    "emailAddresses": [
      "email00@example.com",
      "email10@example.com",
      "email20@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike1",
    "lastName": "Cunneen1",
    "nickName": "Mikey1",
    "title": "Mr",
    "emailAddresses": [
      "email01@example.com",
      "email11@example.com",
      "email21@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike2",
    "lastName": "Cunneen2",
    "nickName": "Mikey2",
    "title": "Mr",
    "emailAddresses": [
      "email02@example.com",
      "email12@example.com",
      "email22@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike3",
    "lastName": "Cunneen3",
    "nickName": "Mikey3",
    "title": "Mr",
    "emailAddresses": [
      "email03@example.com",
      "email13@example.com",
      "email23@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike4",
    "lastName": "Cunneen4",
    "nickName": "Mikey4",
    "title": "Mr",
    "emailAddresses": [
      "email04@example.com",
      "email14@example.com",
      "email24@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike5",
    "lastName": "Cunneen5",
    "nickName": "Mikey5",
    "title": "Mr",
    "emailAddresses": [
      "email05@example.com",
      "email15@example.com",
      "email25@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike6",
    "lastName": "Cunneen6",
    "nickName": "Mikey6",
    "title": "Mr",
    "emailAddresses": [
      "email06@example.com",
      "email16@example.com",
      "email26@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike7",
    "lastName": "Cunneen7",
    "nickName": "Mikey7",
    "title": "Mr",
    "emailAddresses": [
      "email07@example.com",
      "email17@example.com",
      "email27@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike8",
    "lastName": "Cunneen8",
    "nickName": "Mikey8",
    "title": "Mr",
    "emailAddresses": [
      "email08@example.com",
      "email18@example.com",
      "email28@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  },
  {
    "firstName": "Mike9",
    "lastName": "Cunneen9",
    "nickName": "Mikey9",
    "title": "Mr",
    "emailAddresses": [
      "email09@example.com",
      "email19@example.com",
      "email29@example.com"
    ],
    "mailFormat": "\u0000",
    "displayFormat": 0
  }
]